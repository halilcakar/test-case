import { computed } from 'vue'
import { useStore } from 'vuex'

/**
 * Small helper function like React.useState()
 */

export function useState(arr) {
	if (!Array.isArray(arr)) {
		arr = [arr]
	}
	const store = useStore()

	const pair = arr.map((s) => [s, computed(() => store.state[s])])

	return Object.fromEntries(pair)
}

export function useGetters(arr) {
	if (!Array.isArray(arr)) {
		arr = [arr]
	}
	const store = useStore()

	const pair = arr.map((g) => [g, computed(() => store.getters[g])])

	return Object.fromEntries(pair)
}

export function useMutations(arr) {
	if (!Array.isArray(arr)) {
		arr = [arr]
	}
	const store = useStore()

	const pair = arr.map((m) => [m, (input) => store.commit(m, input)])

	return Object.fromEntries(pair)
}

export const sortBy = {
	general: (a, z) => {
		let dif = z.createdAt - a.createdAt

		if (dif === 0) {
			return z.updatedAt - a.updatedAt
		}

		return dif
	},
	desc: (a, z) => {
		let dif = z.votes - a.votes

		if (dif === 0) {
			return z.updatedAt - a.updatedAt
		}

		return dif
	},
	asc: (a, z) => {
		let dif = a.votes - z.votes

		if (dif === 0) {
			return a.updatedAt - z.updatedAt
		}

		return dif
	},
}
