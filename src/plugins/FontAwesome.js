import { library } from '@fortawesome/fontawesome-svg-core'
import {
	faTrash,
	faArrowUp,
	faArrowDown,
	faArrowLeft,
	faLightbulb,
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faTrash, faArrowUp, faArrowDown, faArrowLeft, faLightbulb)

export const Plugin = {
	install: (app) => {
		app.component('fa-icon', FontAwesomeIcon)
	},
}
