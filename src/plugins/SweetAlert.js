import { Toast, SwalSymbol, ToastSymbol } from '../hooks/useSwal'
import Swal from 'sweetalert2'

export const Plugin = {
	install: (app) => {
		app.config.globalProperties.$Swal = Swal
		app.config.globalProperties.$Toast = Toast
		app.provide(SwalSymbol, Swal)
		app.provide(ToastSymbol, Toast)
	},
}
