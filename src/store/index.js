import { createStore } from 'vuex'
import { v4 as uuid } from 'uuid'
import { sortBy } from '../helpers'

export const LOCAL_STORAGE_KEY = 'HEPSIBURADA_LOCAL_DATA'

export default createStore({
	state() {
		return {
			list: [
				// {
				// 	id: uuid(),
				// 	title: 'Hacker News',
				// 	url: 'https://news.ycombinator.com',
				// 	votes: 18,
				// 	createdAt: new Date(Date.now() - 24 * 3600 * 1000),
				// 	updatedAt: new Date(Date.now() - 24 * 3600 * 1000),
				// },
				// {
				// 	id: uuid(),
				// 	title: 'Hepsiburada',
				// 	url: 'https://hepsiburada.com',
				// 	votes: 26,
				// 	createdAt: new Date(),
				// 	updatedAt: new Date(),
				// },
				// {
				// 	id: uuid(),
				// 	title: 'Product Hunt',
				// 	url: 'https://producthunt.com',
				// 	votes: 22,
				// 	createdAt: new Date(),
				// 	updatedAt: new Date(),
				// },
				// {
				// 	id: uuid(),
				// 	title: 'Reddit',
				// 	url: 'https://reddit.com',
				// 	votes: 4,
				// 	createdAt: new Date(Date.now() - 48 * 3600 * 1000),
				// 	updatedAt: new Date(Date.now() - 48 * 3600 * 1000),
				// },
				// {
				// 	id: uuid(),
				// 	title: 'Google',
				// 	url: 'https://google.com',
				// 	votes: 2,
				// 	createdAt: new Date(),
				// 	updatedAt: new Date(),
				// },
				// {
				// 	id: uuid(),
				// 	title: 'DuckDuckGo',
				// 	url: 'https://duckduckgo.com',
				// 	votes: 3,
				// 	createdAt: new Date(),
				// 	updatedAt: new Date(),
				// },
			],
			orderType: 'general',
			search: '',
			pagination: {
				page: 1,
				limit: 5,
				totalPages: 0,
			},
		}
	},
	getters: {
		getList(state) {
			state.pagination.totalPages = Math.ceil(
				state.list.length / state.pagination.limit
			)
			let list = state.list.slice().sort(sortBy[state.orderType])

			const limit = state.pagination.limit
			const pagination = state.pagination

			if (state.search !== '') {
				list = list.filter(
					(item) =>
						item.title.includes(state.search) || item.url.includes(state.search)
				)
				state.pagination.totalPages = Math.ceil(
					list.length / state.pagination.limit
				)
			}

			let start = (pagination.page - 1) * limit
			if (pagination.page >= state.pagination.totalPages) {
				pagination.page = state.pagination.totalPages
				start = (pagination.page - 1) * limit
			}

			if (start <= 0) {
				start = 0
			}

			return list.slice(start, start + limit)
		},
		getLinkCount(state) {
			return state.list.length
		},
		pagination(state) {
			return state.pagination
		},
	},
	mutations: {
		add(state, payload) {
			state.list.push({
				id: uuid(),
				...payload,
				votes: 0,
				createdAt: new Date(),
				updatedAt: new Date(),
			})
		},
		remove(state, id) {
			state.list = state.list.filter((child) => child.id !== id)
		},
		upVote(state, id) {
			let link = state.list.find((child) => child.id === id)
			if (link) {
				link.updatedAt = new Date()
				link.votes++
			}
		},
		downVote(state, id) {
			let link = state.list.find((child) => child.id === id)
			if (link) {
				link.votes--
				if (link.votes < 0) {
					link.votes = 0
				} else {
					link.updatedAt = new Date()
				}
			}
		},
		setOrderType(state, orderType) {
			state.orderType = orderType
		},
		setSearch(state, search) {
			state.search = search
		},
		setPage(state, page) {
			state.pagination.page = page
		},
		setList(state, list) {
			state.list = list
		},
	},
	actions: {
		put({ state }) {
			// api call to a server or in this case localstorage.
			localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(state.list))
		},
		add({ dispatch, commit }, payload) {
			commit('add', payload)
			dispatch('put')
		},
		remove({ dispatch, commit }, id) {
			commit('remove', id)
			dispatch('put')
		},
		upVote({ dispatch, commit }, id) {
			commit('upVote', id)
			dispatch('put')
		},
		downVote({ dispatch, commit }, id) {
			commit('downVote', id)
			dispatch('put')
		},
	},
})
