import { mount } from '@vue/test-utils'
import { createStore } from 'vuex'
import Filters from '@/components/Filters.vue'
import { Plugin as FontAwesomePlugin } from '@/plugins/FontAwesome'
import { Plugin as SweetAlertPlugin } from '@/plugins/SweetAlert'

describe('Filters.vue', () => {
	let wrapper, store, setOrderTypeFn, setPageFn, setSearchFn

	beforeEach(() => {
		setPageFn = jest.fn()
		setSearchFn = jest.fn()
		setOrderTypeFn = jest.fn()
		store = createStore({
			mutations: {
				setPage: setPageFn,
				setSearch: setSearchFn,
				setOrderType: setOrderTypeFn,
			},
		})
		wrapper = mount(Filters, {
			global: { plugins: [store, FontAwesomePlugin, SweetAlertPlugin] },
		})
	})

	it('Has a select with 3 options', () => {
		expect(wrapper.find('select')).toBeTruthy()
		expect(wrapper.findAll('option')).toHaveLength(3)
	})

	it('commits setOrderType when select change', async () => {
		const select = wrapper.find('select')

		await select.setValue('desc')

		expect(setOrderTypeFn).toHaveBeenCalled()
		expect(setPageFn).toHaveBeenCalled()
	})

	it('commits setSearch when search input change', async () => {
		const input = wrapper.find('[type="search"]')

		await input.setValue('Heps')

		expect(setSearchFn).toHaveBeenCalled()
		expect(setPageFn).toHaveBeenCalled()
	})
})
