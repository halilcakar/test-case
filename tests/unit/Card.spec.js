import { mount, flushPromises } from '@vue/test-utils'
import { createStore } from 'vuex'
import Card from '@/atomics/Card.vue'
import { Plugin as FontAwesomePlugin } from '@/plugins/FontAwesome'
import { Plugin as SweetAlertPlugin } from '@/plugins/SweetAlert'

describe('Card.vue', () => {
	let wrapper, store, upVoteFn, downVoteFn, removeFn
	const data = {
		id: 'test-uuid',
		title: 'Hepsiburada',
		url: 'https://hepsiburada.com',
		votes: 26,
		createdAt: new Date(),
		updatedAt: new Date(),
	}

	beforeEach(() => {
		removeFn = jest.fn()
		upVoteFn = jest.fn()
		downVoteFn = jest.fn()
		store = createStore({
			actions: {
				remove: removeFn,
				upVote: upVoteFn,
				downVote: downVoteFn,
			},
		})
		wrapper = mount(Card, {
			props: { data },
			global: { plugins: [store, FontAwesomePlugin, SweetAlertPlugin] },
		})
	})

	it("renders it's [title, url, votes] when passed", async () => {
		expect(wrapper.text()).toContain(data.title)
		expect(wrapper.text()).toContain(data.url)
		expect(wrapper.text()).toContain(data.votes.toString())
	})

	it('calls upVote action when clicked', async () => {
		const button = wrapper.find('#up-vote')
		await button.trigger('click')
		expect(upVoteFn).toHaveBeenCalled()
	})

	it('calls downVote action when clicked', async () => {
		const button = wrapper.find('#down-vote')
		await button.trigger('click')
		expect(downVoteFn).toHaveBeenCalled()
	})

	/**
	 * Jest nedense sweetalert'in promise'larını mock edemediği
	 * için (Ya da ben doğru kullanmıyorum ama bildiğim kadarıyla bu şekilde)
	 * maalesef alttaki test fail oluyor.
	 */
	jest.mock('sweetalert2', () => ({
		get: jest.fn(() => Promise.resolve({ isConfirmed: true })),
	}))
	it.skip('calls remove action when clicked', async () => {
		const button = wrapper.find('[data-testid="card-remove"]')

		await button.trigger('click')
		await flushPromises()

		expect(removeFn).toHaveBeenCalled()
	})
})
